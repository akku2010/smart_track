webpackJsonp([8],{

/***/ 579:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OverSpeedRepoPageModule", function() { return OverSpeedRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__ = __webpack_require__(652);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(653);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var OverSpeedRepoPageModule = /** @class */ (function () {
    function OverSpeedRepoPageModule() {
    }
    OverSpeedRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__over_speed_repo__["a" /* OverSpeedRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], OverSpeedRepoPageModule);
    return OverSpeedRepoPageModule;
}());

//# sourceMappingURL=over-speed-repo.module.js.map

/***/ }),

/***/ 652:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverSpeedRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var OverSpeedRepoPage = /** @class */ (function () {
    function OverSpeedRepoPage(navCtrl, navParams, apicalloverspeed, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalloverspeed = apicalloverspeed;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.overspeedReport = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    OverSpeedRepoPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    OverSpeedRepoPage.prototype.change = function (datetimeStart) {
        console.log(datetimeStart);
    };
    OverSpeedRepoPage.prototype.change1 = function (datetimeEnd) {
        console.log(datetimeEnd);
    };
    OverSpeedRepoPage.prototype.getOverspeeddevice = function (selectedVehicle) {
        debugger;
        // console.log("selectedVehicle=> ", selectedVehicle)
        this.overSpeeddevice_id = selectedVehicle.Device_ID;
        // console.log("selected vehicle=> " + this.overSpeeddevice_id)
    };
    OverSpeedRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicalloverspeed.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicalloverspeed.stopLoading();
            console.log(err);
        });
    };
    OverSpeedRepoPage.prototype.getOverSpeedReport = function (starttime, endtime) {
        var _this = this;
        var baseUrl;
        if (this.overSpeeddevice_id == undefined) {
            baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id;
        }
        else {
            baseUrl = this.apicalloverspeed.mainUrl + "notifs/overSpeedReport?from_date=" + new Date(starttime).toISOString() + '&to_date=' + new Date(endtime).toISOString() + '&_u=' + this.islogin._id + '&device=' + this.overSpeeddevice_id;
        }
        this.apicalloverspeed.startLoading().present();
        this.apicalloverspeed.getOverSpeedApi(baseUrl)
            .subscribe(function (data) {
            _this.apicalloverspeed.stopLoading();
            // this.overspeedReport = data;
            if (data.length > 0) {
                _this.innerFunc(data);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicalloverspeed.stopLoading();
            console.log(error);
        });
    };
    OverSpeedRepoPage.prototype.innerFunc = function (overspeedReport) {
        var outerthis = this;
        var i = 0, howManyTimes = overspeedReport.length;
        function f() {
            outerthis.overspeedReport.push({
                'vehicleName': overspeedReport[i].vehicleName,
                'overSpeed': overspeedReport[i].overSpeed,
                'start_location': {
                    'lat': overspeedReport[i].lat,
                    'long': overspeedReport[i].long
                },
            });
            outerthis.start_address(outerthis.overspeedReport[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    OverSpeedRepoPage.prototype.start_address = function (item, index) {
        var that = this;
        that.overspeedReport[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.overspeedReport[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                that.overspeedReport[index].StartLocation = str;
            });
        }
    };
    OverSpeedRepoPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicalloverspeed.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    OverSpeedRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-over-speed-repo',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/over-speed-repo/over-speed-repo.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Over Speed Report\' | translate}}</ion-title>\n  </ion-navbar>\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name" itemTextField="Device_Name"\n      [canSearch]="true" (onChange)="getOverspeeddevice(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n\n\n  <ion-row style="background-color: #fafafa;" padding-left padding-right>\n    <ion-col width-20>\n\n      <ion-label>\n        <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart" (ionChange)="change(datetimeStart)"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd" (ionChange)="change1(datetimeEnd)"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getOverSpeedReport(datetimeStart,datetimeEnd);"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <ion-card *ngFor="let overdata of overspeedReport; let i=index">\n    <ion-item style="border-bottom: 2px solid #dedede;">\n      <ion-avatar item-start>\n        <img src="assets/imgs/car_red_icon.png">\n      </ion-avatar>\n      <ion-row>\n        <ion-col col-8>\n          <p style="color:gray;font-size:13px;">{{overdata.vehicleName}}</p>\n        </ion-col>\n        <ion-col col-4>\n          <p>\n            <ion-icon ios="ios-speedometer" md="md-speedometer" style="color:#e14444;"></ion-icon>&nbsp;\n            <span style="font-size:12px;">{{overdata.overSpeed}}&nbsp;</span>\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="padding-top: 13px;">\n        <ion-col (onCreate)="start_address(overdata, i)"  style="font-size: 0.8em;">\n          <div class="overme">\n            {{ overdata.StartLocation }}\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/over-speed-repo/over-speed-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], OverSpeedRepoPage);
    return OverSpeedRepoPage;
}());

//# sourceMappingURL=over-speed-repo.js.map

/***/ }),

/***/ 653:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=8.js.map