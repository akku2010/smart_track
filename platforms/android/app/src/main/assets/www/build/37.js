webpackJsonp([37],{

/***/ 569:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeofenceReportPageModule", function() { return GeofenceReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__geofence_report__ = __webpack_require__(643);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var GeofenceReportPageModule = /** @class */ (function () {
    function GeofenceReportPageModule() {
    }
    GeofenceReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__geofence_report__["a" /* GeofenceReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
        })
    ], GeofenceReportPageModule);
    return GeofenceReportPageModule;
}());

//# sourceMappingURL=geofence-report.module.js.map

/***/ }),

/***/ 643:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GeofenceReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var GeofenceReportPage = /** @class */ (function () {
    function GeofenceReportPage(navCtrl, navParams, apicallGeofenceReport, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallGeofenceReport = apicallGeofenceReport;
        this.toastCtrl = toastCtrl;
        this.geofenceRepoert = [];
        this.geofenceReportdata = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    GeofenceReportPage.prototype.ngOnInit = function () {
        this.getgeofence1();
    };
    GeofenceReportPage.prototype.getgeofence1 = function () {
        var _this = this;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getallgeofenceCall(this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofencelist = data;
            console.log("geofencelist=> ", _this.geofencelist);
        }, function (err) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(err);
        });
    };
    GeofenceReportPage.prototype.getGeofencedata = function (geofence) {
        console.log("selectedVehicle=> ", geofence);
        this.Ignitiondevice_id = geofence._id;
        this.getGeofenceReport();
    };
    GeofenceReportPage.prototype.getGeofenceReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicallGeofenceReport.startLoading().present();
        this.apicallGeofenceReport.getGeogenceReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallGeofenceReport.stopLoading();
            _this.geofenceRepoert = data;
            if (_this.geofenceRepoert.length > 0) {
                _this.innerFunc(_this.geofenceRepoert);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not fond for selected Dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallGeofenceReport.stopLoading();
            console.log(error);
        });
    };
    GeofenceReportPage.prototype.innerFunc = function (geofenceRepoert) {
        var outerthis = this;
        outerthis.geofenceReportdata = [];
        var i = 0, howManyTimes = geofenceRepoert.length;
        function f() {
            outerthis.locationEndAddress = undefined;
            outerthis.geofenceReportdata.push({
                'device': outerthis.geofenceRepoert[i].device,
                'vehicleName': outerthis.geofenceRepoert[i].vehicleName,
                'direction': outerthis.geofenceRepoert[i].direction,
                'timestamp': outerthis.geofenceRepoert[i].timestamp,
                '_id': outerthis.geofenceRepoert[i]._id,
                'start_location': {
                    'lat': outerthis.geofenceRepoert[i].lat,
                    'long': outerthis.geofenceRepoert[i].long
                }
            });
            outerthis.start_address(outerthis.geofenceReportdata[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    GeofenceReportPage.prototype.start_address = function (item, index) {
        var that = this;
        that.geofenceReportdata[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.geofenceReportdata[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            var payload = {
                "lat": item.start_location.lat,
                "long": item.start_location.long,
                "api_id": "2"
            };
            this.apicallGeofenceReport.getAddressApi(payload)
                .subscribe(function (data) {
                console.log("got address: " + data.results);
                if (data.results[2] != undefined || data.results[2] != null) {
                    that.geofenceReportdata[index].StartLocation = data.results[2].formatted_address;
                }
                else {
                    that.geofenceReportdata[index].StartLocation = 'N/A';
                }
            });
        }
    };
    GeofenceReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-geofence-report',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/geofence-report/geofence-report.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n            <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>{{\'Geofence Report\' | translate}}</ion-title>\n    </ion-navbar>\n    <ion-item style="background-color: #fafafa;">\n        <ion-label style="margin-top: 15px;">{{\'Select Geofence\' | translate}}</ion-label>\n        <select-searchable item-content [(ngModel)]="selectedGeofence" [items]="geofencelist" itemValueField="geoname"\n            itemTextField="geoname" [canSearch]="true" (onChange)="getGeofencedata(selectedGeofence)">\n        </select-searchable>\n    </ion-item>\n\n    <ion-row style="background-color: #fafafa;" padding-left padding-right>\n        <ion-col width-20>\n\n            <ion-label>\n                <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n                    [(ngModel)]="datetimeStart" style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n                </ion-datetime>\n            </ion-label>\n        </ion-col>\n\n        <ion-col width-20>\n            <ion-label>\n                <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n                <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a"\n                    [(ngModel)]="datetimeEnd" style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n                </ion-datetime>\n            </ion-label>\n        </ion-col>\n\n        <ion-col width-20>\n            <div style="margin-top: 9px; float: right">\n                <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getGeofenceReport();">\n                </ion-icon>\n            </div>\n        </ion-col>\n    </ion-row>\n</ion-header>\n\n<ion-content>\n    <ion-card *ngFor="let item of geofenceReportdata;">\n        <ion-item style="border-bottom: 2px solid #dedede;">\n            <ion-avatar item-start>\n                <img src="assets/imgs/car_red_icon.png"\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'0\'))">\n                <img src="assets/imgs/car_green_icon.png"\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==\'1\'))">\n                <img src="assets/imgs/car_grey_icon.png"\n                    *ngIf="((item.device.iconType == \'car\')&&(item.device.last_ACC==null))">\n\n                <img src="assets/imgs/truck_icon_red.png"\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'0\'))">\n                <img src="assets/imgs/truck_icon_green.png"\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==\'1\'))">\n                <img src="assets/imgs/truck_icon_grey.png"\n                    *ngIf="((item.device.iconType == \'truck\')&&(item.device.last_ACC==null))">\n\n                <img src="assets/imgs/bike_red_icon.png"\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'0\'))">\n                <img src="assets/imgs/bike_green_icon.png"\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==\'1\'))">\n                <img src="assets/imgs/bike_grey_icon.png"\n                    *ngIf="((item.device.iconType == \'bike\')&&(item.device.last_ACC==null))">\n\n                <img src="assets/imgs/jcb_red.png"\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'0\'))">\n                <img src="assets/imgs/jcb_green.png"\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==\'1\'))">\n                <img src="assets/imgs/jcb_gray.png"\n                    *ngIf="((item.device.iconType == \'jcb\')&&(item.device.last_ACC==null))">\n\n                <img src="assets/imgs/bus_red.png"\n                    *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'0\'))">\n                <img src="assets/imgs/bus_green.png"\n                    *ngIf="((item.device.iconType == \'bus\')&&(item.device.last_ACC==\'1\'))">\n                <img src="assets/imgs/bus_gray.png"\n                    *ngIf="((item.device.iconType == \'bus\')&&( !item.device.last_ACC ))">\n\n            </ion-avatar>\n            <ion-label>{{item.vehicleName}}</ion-label>\n            <ion-badge item-end color="gpsc" *ngIf="item.direction==\'Out\'">{{item.direction}}&nbsp; <ion-icon\n                    ios="ios-arrow-round-up" md="md-arrow-round-up"></ion-icon>\n            </ion-badge>\n            <ion-badge item-end color="secondary" *ngIf="item.direction==\'In\'">{{item.direction}}&nbsp;<ion-icon\n                    ios="ios-arrow-round-down" md="md-arrow-round-down"></ion-icon>\n            </ion-badge>\n        </ion-item>\n        <ion-card-content>\n            <ion-row style="padding-top:5px;">\n                <ion-col col-1>\n                    <ion-icon ios="ios-time" md="md-time" style="font-size:15px;"></ion-icon>\n                </ion-col>\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n                    {{item.timestamp | date: \'medium\'}}\n                </ion-col>\n            </ion-row>\n            <ion-row>\n                <ion-col col-1>\n                    <ion-icon ios="ios-pin" md="md-pin" color="gpsc" style="font-size:15px;"></ion-icon>\n                </ion-col>\n                <ion-col col-11 style="color:gray;font-size:11px;font-weight: 400;">\n                    {{item.StartLocation}}\n                </ion-col>\n            </ion-row>\n        </ion-card-content>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/geofence-report/geofence-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"]])
    ], GeofenceReportPage);
    return GeofenceReportPage;
}());

//# sourceMappingURL=geofence-report.js.map

/***/ })

});
//# sourceMappingURL=37.js.map