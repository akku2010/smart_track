webpackJsonp([32],{

/***/ 570:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDevicePageModule", function() { return HistoryDevicePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__history_device__ = __webpack_require__(644);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// import { ModalPage } from './modal';
var HistoryDevicePageModule = /** @class */ (function () {
    function HistoryDevicePageModule() {
    }
    HistoryDevicePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__history_device__["a" /* HistoryDevicePage */]),
                __WEBPACK_IMPORTED_MODULE_3__node_modules_ion_bottom_drawer__["b" /* IonBottomDrawerModule */],
                __WEBPACK_IMPORTED_MODULE_4_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            schemas: [__WEBPACK_IMPORTED_MODULE_0__angular_core__["CUSTOM_ELEMENTS_SCHEMA"]],
        })
    ], HistoryDevicePageModule);
    return HistoryDevicePageModule;
}());

//# sourceMappingURL=history-device.module.js.map

/***/ }),

/***/ 644:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoryDevicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(66);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__modal__ = __webpack_require__(397);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HistoryDevicePage = /** @class */ (function () {
    function HistoryDevicePage(events, navCtrl, navParams, alertCtrl, toastCtrl, apiCall, plt, translate, modalCtrl) {
        this.events = events;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.apiCall = apiCall;
        this.plt = plt;
        this.translate = translate;
        this.modalCtrl = modalCtrl;
        this.shouldBounce = true;
        this.dockedHeight = 100;
        this.distanceTop = 378;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 0;
        this.showActionSheet = false;
        this.transition = ['0.5s', 'ease-in-out'];
        this.locations = [];
        this.SelectVehicle = 'Select Vehicle';
        this.allData = {};
        this.showZoom = false;
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        console.log('start date', this.datetimeStart);
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        console.log('stop date', this.datetimeEnd);
    }
    HistoryDevicePage.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem("SCREEN") != null) {
            this.navBar.backButtonClick = function (e) {
                // todo something
                // this.navController.pop();
                console.log("back button poped");
                if (localStorage.getItem("SCREEN") != null) {
                    if (localStorage.getItem("SCREEN") === 'live') {
                        _this.navCtrl.setRoot('LivePage');
                    }
                    else {
                        if (localStorage.getItem("SCREEN") === 'dashboard') {
                            _this.navCtrl.setRoot('DashboardPage');
                        }
                    }
                }
            };
        }
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        if (localStorage.getItem("MainHistory") != null) {
            console.log("coming soon");
            this.showDropDown = true;
            this.getdevices();
        }
        else {
            this.device = this.navParams.get('device');
            console.log("devices=> ", this.device);
            this.trackerId = this.device.Device_ID;
            this.trackerType = this.device.iconType;
            this.DeviceId = this.device._id;
            this.trackerName = this.device.Device_Name;
            this.btnClicked(this.datetimeStart, this.datetimeEnd);
        }
        this.hideplayback = false;
        this.target = 0;
    };
    HistoryDevicePage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("markerTarget");
        localStorage.removeItem("speedMarker");
        localStorage.removeItem("updatetimedate");
        localStorage.removeItem("MainHistory");
    };
    HistoryDevicePage.prototype.changeformat = function (date) {
        console.log("date=> " + new Date(date).toISOString());
    };
    HistoryDevicePage.prototype.setDocHeight = function () {
        console.log("dockerchage event");
        this.dockedHeight = 150;
        this.distanceTop = 46;
    };
    HistoryDevicePage.prototype.closeDocker = function () {
        var that = this;
        that.showActionSheet = false;
    };
    HistoryDevicePage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apiCall.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apiCall.startLoading().present();
        this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            _this.portstemp = data.devices;
            // this.devices1243 = [];
            // this.devices = data;
            // this.devices1243.push(data);
            // localStorage.setItem('devices', this.devices);
            // this.isdevice = localStorage.getItem('devices');
            // for (var i = 0; i < this.devices1243[i]; i++) {
            //   this.devices1243[i] = {
            //     'color': ('#' + Math.floor(Math.random() * 16777215).toString(16))
            //   };
            // }
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log(error);
        });
    };
    HistoryDevicePage.prototype.onChangedSelect = function (item) {
        var that = this;
        that.trackerId = item.Device_ID;
        that.trackerType = item.iconType;
        that.DeviceId = item._id;
        that.trackerName = item.Device_Name;
        if (that.allData.map) {
            that.allData.map.clear();
            that.allData.map.remove();
        }
    };
    HistoryDevicePage.prototype.Playback = function () {
        var that = this;
        that.showZoom = true;
        if (localStorage.getItem("markerTarget") != null) {
            that.target = JSON.parse(localStorage.getItem("markerTarget"));
        }
        that.playing = !that.playing; // This would alternate the state each time
        var coord = that.dataArrayCoords[that.target];
        that.coordreplaydata = coord;
        var lat = coord[0];
        var lng = coord[1];
        that.startPos = [lat, lng];
        that.speed = 200; // km/h
        if (that.playing) {
            that.allData.map.setCameraTarget({ lat: lat, lng: lng });
            if (that.allData.mark == undefined) {
                var icicon;
                if (that.plt.is('ios')) {
                    icicon = 'www/assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                else if (that.plt.is('android')) {
                    icicon = './assets/imgs/vehicles/running' + that.trackerType + '.png';
                }
                that.allData.map.addMarker({
                    icon: icicon,
                    styles: {
                        'text-align': 'center',
                        'font-style': 'italic',
                        'font-weight': 'bold',
                        'color': 'green'
                    },
                    position: new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]),
                }).then(function (marker) {
                    that.allData.mark = marker;
                    that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
                });
            }
            else {
                that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
                that.liveTrack(that.allData.map, that.allData.mark, that.dataArrayCoords, that.target, that.startPos, that.speed, 100);
            }
        }
        else {
            that.allData.mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](that.startPos[0], that.startPos[1]));
        }
    };
    HistoryDevicePage.prototype.liveTrack = function (map, mark, coords, target, startPos, speed, delay) {
        var that = this;
        that.events.subscribe("SpeedValue:Updated", function (sdata) {
            speed = sdata;
        });
        var target = target;
        if (!startPos.length)
            coords.push([startPos[0], startPos[1]]);
        function _gotoPoint() {
            if (target > coords.length)
                return;
            var lat = mark.getPosition().lat;
            var lng = mark.getPosition().lng;
            var step = (speed * 1000 * delay) / 3600000;
            if (coords[target] == undefined)
                return;
            var dest = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](coords[target][0], coords[target][1]);
            var distance = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeDistanceBetween(dest, mark.getPosition()); //in meters
            var numStep = distance / step;
            var i = 0;
            var deltaLat = (coords[target][0] - lat) / numStep;
            var deltaLng = (coords[target][1] - lng) / numStep;
            function changeMarker(mark, deg) {
                mark.setRotation(deg);
            }
            function _moveMarker() {
                lat += deltaLat;
                lng += deltaLng;
                i += step;
                var head;
                if (i < distance) {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    map.setCameraTarget(new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](lat, lng));
                    setTimeout(_moveMarker, delay);
                }
                else {
                    head = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["h" /* Spherical */].computeHeading(mark.getPosition(), dest);
                    if ((head != 0) || (head == NaN)) {
                        changeMarker(mark, head);
                    }
                    mark.setPosition(dest);
                    map.setCameraTarget(dest);
                    target++;
                    setTimeout(_gotoPoint, delay);
                }
            }
            a++;
            if (a > coords.length) {
            }
            else {
                that.speedMarker = coords[target][3].speed;
                that.updatetimedate = coords[target][2].time;
                if (that.playing) {
                    _moveMarker();
                    target = target;
                    localStorage.setItem("markerTarget", target);
                }
                else { }
                // km_h = km_h;
            }
        }
        var a = 0;
        _gotoPoint();
    };
    HistoryDevicePage.prototype.zoomin = function () {
        var that = this;
        that.allData.map.moveCameraZoomIn();
    };
    HistoryDevicePage.prototype.zoomout = function () {
        var that = this;
        that.allData.map.animateCameraZoomOut();
    };
    HistoryDevicePage.prototype.inter = function (fastforwad) {
        // debugger
        var that = this;
        console.log("fastforwad=> " + fastforwad);
        if (fastforwad == 'fast') {
            that.speed = 2 * that.speed;
            console.log("speed fast=> " + that.speed);
        }
        else if (fastforwad == 'slow') {
            if (that.speed > 50) {
                that.speed = that.speed / 2;
                console.log("speed slow=> " + that.speed);
            }
            else {
                console.log("speed normal=> " + that.speed);
            }
        }
        else {
            that.speed = 200;
        }
        that.events.publish("SpeedValue:Updated", that.speed);
    };
    HistoryDevicePage.prototype.btnClicked = function (timeStart, timeEnd) {
        if (localStorage.getItem("MainHistory") != null) {
            if (this.selectedVehicle == undefined) {
                var alert_1 = this.alertCtrl.create({
                    message: "Please select the vehicle first!!",
                    buttons: ['OK']
                });
                alert_1.present();
            }
            else {
                this.maphistory(timeStart, timeEnd);
            }
        }
        else {
            this.maphistory(timeStart, timeEnd);
        }
    };
    HistoryDevicePage.prototype.maphistory = function (timeStart, timeEnd) {
        var _this = this;
        var from1 = new Date(timeStart);
        this.fromtime = from1.toISOString();
        var to1 = new Date(timeEnd);
        this.totime = to1.toISOString();
        if (this.totime >= this.fromtime) {
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: 'Select Correct Time',
                message: 'To time always greater than From Time',
                buttons: ['ok']
            });
            alert_2.present();
            return false;
        }
        this.apiCall.startLoading().present();
        this.apiCall.getDistanceSpeedCall(this.trackerId, this.fromtime, this.totime)
            .subscribe(function (data3) {
            _this.data2 = data3;
            _this.latlongObjArr = data3;
            if (_this.data2["Average Speed"] == 'NaN') {
                _this.data2.AverageSpeed = 0;
            }
            else {
                _this.data2.AverageSpeed = _this.data2["Average Speed"];
            }
            _this.data2.IdleTime = _this.data2["Idle Time"];
            _this.hideplayback = true;
            // this.customTxt = "<html> <head><style> </style> </head><body>Total Distance - " + this.total_dis + " Km<br>Average Speed - " + this.avg_speed + " Km/hr</body> </html> "
            //////////////////////////////////
            _this.callgpsFunc(_this.fromtime, _this.totime);
            // this.locations = [];
            // this.stoppages(timeStart, timeEnd);
            ////////////////////////////////
        }, function (error) {
            _this.apiCall.stopLoading();
            console.log("error in getdistancespeed =>  ", error);
            var body = error._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.stoppages = function () {
        var _this = this;
        this.locations = [];
        var that = this;
        that.apiCall.stoppage_detail(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.DeviceId)
            .subscribe(function (res) {
            console.log('stoppage data', res);
            var arr = [];
            for (var i = 0; i < res.length; i++) {
                _this.arrivalTime = new Date(res[i].arrival_time).toLocaleString();
                _this.departureTime = new Date(res[i].departure_time).toLocaleString();
                var fd = new Date(_this.arrivalTime).getTime();
                var td = new Date(_this.departureTime).getTime();
                var time_difference = td - fd;
                var total_min = time_difference / 60000;
                var hours = total_min / 60;
                var rhours = Math.floor(hours);
                var minutes = (hours - rhours) * 60;
                var rminutes = Math.round(minutes);
                var Durations = rhours + 'Hours' + ':' + rminutes + 'Min';
                arr.push({
                    lat: res[i].lat,
                    lng: res[i].long,
                    arrival_time: res[i].arrival_time,
                    departure_time: res[i].departure_time,
                    device: res[i].device,
                    address: res[i].address,
                    user: res[i].user,
                    duration: Durations
                });
                that.locations.push(arr);
                if (that.locations[0] != undefined) {
                    for (var k = 0; k < that.locations[0].length; k++) {
                        that.setStoppages(that.locations[0][k]);
                    }
                }
            }
            console.log('stoppage data locations', that.locations);
            // this.callgpsFunc(this.fromtime, this.totime);
        }, function (err) {
            _this.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = _this.alertCtrl.create({
                message: msg.message,
                buttons: ['okay']
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.callgpsFunc = function (fromtime, totime) {
        var _this = this;
        var that = this;
        that.apiCall.gpsCall(this.trackerId, fromtime, totime)
            .subscribe(function (data3) {
            that.apiCall.stopLoading();
            if (data3.length > 0) {
                if (data3.length > 1) {
                    that.gps(data3.reverse());
                }
                else {
                    var alert_3 = that.alertCtrl.create({
                        message: 'No Data found for selected vehicle..',
                        buttons: [{
                                text: 'OK',
                                handler: function () {
                                    // that.datetimeStart = moment({ hours: 0 }).format();
                                    // console.log('start date', this.datetimeStart)
                                    // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                    // console.log('stop date', this.datetimeEnd);
                                    // that.selectedVehicle = undefined;
                                    that.hideplayback = false;
                                }
                            }]
                    });
                    alert_3.present();
                }
            }
            else {
                var alert_4 = that.alertCtrl.create({
                    message: 'No Data found for selected vehicle..',
                    buttons: [{
                            text: 'OK',
                            handler: function () {
                                // that.datetimeStart = moment({ hours: 0 }).format();
                                // console.log('start date', this.datetimeStart)
                                // that.datetimeEnd = moment().format();//new Date(a).toISOString();
                                // console.log('stop date', this.datetimeEnd);
                                // that.selectedVehicle = undefined;
                                that.hideplayback = false;
                            }
                        }]
                });
                alert_4.present();
            }
        }, function (err) {
            that.apiCall.stopLoading();
            var body = err._body;
            var msg = JSON.parse(body);
            var alert = that.alertCtrl.create({
                message: msg.message,
                buttons: [_this.translate.instant('Okay')]
            });
            alert.present();
        });
    };
    HistoryDevicePage.prototype.gps = function (data3) {
        var that = this;
        that.latlongObjArr = data3;
        that.dataArrayCoords = [];
        for (var i = 0; i < data3.length; i++) {
            if (data3[i].lat && data3[i].lng) {
                var arr = [];
                var startdatetime = new Date(data3[i].insertionTime);
                arr.push(data3[i].lat);
                arr.push(data3[i].lng);
                arr.push({ "time": startdatetime.toLocaleString() });
                arr.push({ "speed": data3[i].speed });
                that.dataArrayCoords.push(arr);
            }
        }
        that.mapData = [];
        that.mapData = data3.map(function (d) {
            return { lat: d.lat, lng: d.lng };
        });
        that.mapData.reverse();
        if (that.allData.map != undefined) {
            that.allData.map.remove();
        }
        var bounds = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
        var mapOptions = {
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: that.mapKey
        };
        that.allData.map = __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        that.allData.map.moveCamera({
            target: bounds
        });
        this.allData.map.on(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MAP_CLICK).subscribe(function (data) {
            console.log('Click MAP');
            that.drawerHidden1 = true;
        });
        // if (that.locations[0] != undefined) {              // check if there is stoppages or not
        //   for (var k = 0; k < that.locations[0].length; k++) {
        //     that.setStoppages(that.locations[0][k]);
        //   }
        // }
        that.allData.map.addMarker({
            title: 'D',
            position: that.mapData[0],
            icon: 'red',
            styles: {
                'text-align': 'center',
                'font-style': 'italic',
                'font-weight': 'bold',
                'color': 'red'
            },
        }).then(function (marker) {
            marker.showInfoWindow();
            that.allData.map.addMarker({
                title: 'S',
                position: that.mapData[that.mapData.length - 1],
                icon: 'green',
                styles: {
                    'text-align': 'center',
                    'font-style': 'italic',
                    'font-weight': 'bold',
                    'color': 'green'
                },
            }).then(function (marker) {
                marker.showInfoWindow();
            });
        });
        that.allData.map.addPolyline({
            points: that.mapData,
            color: '#635400',
            width: 3,
            geodesic: true
        });
    };
    HistoryDevicePage.prototype.setStoppages = function (pdata) {
        var that = this;
        ///////////////////////////////
        // let htmlInfoWindow = new HtmlInfoWindow();
        // let frame: HTMLElement = document.createElement('div');
        // frame.innerHTML = [
        //   '<p style="font-size: 7px;">Address:- ' + pdata.address + '</p>',
        //   '<p style="font-size: 7px;">Arrival Time:- ' + moment(new Date(pdata.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>',
        //   '<p style="font-size: 7px;">Departure Time:- ' + moment(new Date(pdata.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a") + '</p>'
        // ].join("");
        // htmlInfoWindow.setContent(frame, { width: "220px", height: "100px" });
        ///////////////////////////////////////////////////
        if (pdata != undefined)
            (function (data) {
                console.log("inside for data=> ", data);
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](centerMarker.lat, centerMarker.lng);
                var markicon;
                if (that.plt.is('ios')) {
                    markicon = 'www/assets/imgs/park.png';
                }
                else if (that.plt.is('android')) {
                    markicon = './assets/imgs/park.png';
                }
                var markerOptions = {
                    position: location,
                    icon: markicon
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    // console.log('centerMarker.ID' + centerMarker.ID)
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) {
                        that.showActionSheet = true;
                        // that.drawerHidden1 = false;
                        that.drawerState = __WEBPACK_IMPORTED_MODULE_6_ion_bottom_drawer__["a" /* DrawerState */].Docked;
                        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* Geocoder */].geocode({
                            "position": {
                                lat: e[0].lat,
                                lng: e[0].lng
                            }
                        }).then(function (results) {
                            if (results.length == 0) {
                                return null;
                            }
                            that.addressof = results[0].extra.lines[0];
                        });
                        setTimeout(function () {
                            that.address = that.addressof;
                            console.log("pickup location new ", that.address);
                            that.arrTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.arrival_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                            that.depTime = __WEBPACK_IMPORTED_MODULE_3_moment__(new Date(data.departure_time), "YYYY-MM-DD").format("DD/MM/YYYY hh:mm a");
                            var fd = new Date(data.arrival_time).getTime();
                            var td = new Date(data.departure_time).getTime();
                            var time_difference = td - fd;
                            var total_min = time_difference / 60000;
                            var hours = total_min / 60;
                            var rhours = Math.floor(hours);
                            var minutes = (hours - rhours) * 60;
                            var rminutes = Math.round(minutes);
                            that.durations = rhours + 'hours' + ':' + rminutes + 'mins';
                        }, 500);
                    });
                });
            })(pdata);
    };
    HistoryDevicePage.prototype.onIdle = function () {
        this.presentModal();
    };
    HistoryDevicePage.prototype.presentModal = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__modal__["a" /* ModalPage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            console.log("onDidDismiss", data);
            _this.getIdlePoints(data);
        });
    };
    HistoryDevicePage.prototype.getIdlePoints = function (min) {
        var _this = this;
        this.idleLocations = [];
        var urlbase = this.apiCall.mainUrl + 'stoppage/trip_idle?uId=' + this.islogin._id + '&from_date=' + new Date(this.datetimeStart).toISOString() + '&to_date=' + new Date(this.datetimeEnd).toISOString() + '&device=' + this.DeviceId + '&min_time=' + min;
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(urlbase)
            .subscribe(function (data) {
            _this.apiCall.stopLoading();
            console.log("idle data=> " + data);
            if (data.length > 0) {
                for (var y = 0; y <= data.length; y++) {
                    _this.idleLocations.push(data[y]);
                }
                if (_this.idleLocations.length > 0) {
                    for (var k = 0; k < _this.idleLocations.length; k++) {
                        _this.setIdlePoints(_this.idleLocations[k]);
                    }
                }
            }
        });
    };
    HistoryDevicePage.prototype.setIdlePoints = function (pdata) {
        var that = this;
        if (pdata != undefined)
            (function (data) {
                console.log("inside for data=> ", data);
                var centerMarker = data;
                var location = new __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["f" /* LatLng */](centerMarker.idle_location.lat, centerMarker.idle_location.long);
                var markicon;
                if (that.plt.is('ios')) {
                    markicon = 'www/assets/imgs/idle.png';
                }
                else if (that.plt.is('android')) {
                    markicon = './assets/imgs/idle.png';
                }
                var markerOptions = {
                    position: location,
                    icon: markicon
                };
                that.allData.map.addMarker(markerOptions)
                    .then(function (marker) {
                    // console.log('centerMarker.ID' + centerMarker.ID)
                    marker.addEventListener(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK)
                        .subscribe(function (e) { });
                });
            })(pdata);
    };
    HistoryDevicePage.prototype.onClickMainMenu = function (item) {
        this.menuActive = !this.menuActive;
    };
    HistoryDevicePage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE') {
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
        }
        else {
            if (maptype == 'TERRAIN') {
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
            }
            else {
                if (maptype == 'NORMAL') {
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                }
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], HistoryDevicePage.prototype, "navBar", void 0);
    HistoryDevicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-history-device',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/history-device/history-device.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="device">{{ device.Device_Name }}</ion-title>\n    <ion-title *ngIf="!device">{{ "View History" | translate }}</ion-title>\n    <ion-buttons end>\n      <div *ngIf="hideplayback">\n        <ion-icon\n          color="light"\n          name="rewind"\n          style="font-size:19px;margin-top:11px;margin-right: 17px"\n          (click)="inter(\'slow\')"\n        ></ion-icon>\n        <ion-icon\n          color="light"\n          name="arrow-dropright-circle"\n          style="font-size:24px;margin-top:10px;margin-right: 15px"\n          class="play"\n          *ngIf="!playing"\n          (click)="Playback()"\n        ></ion-icon>\n        <ion-icon\n          color="light"\n          name="pause"\n          style="font-size:24px;margin-top:10px;margin-right: 15px"\n          class="pause"\n          *ngIf="playing"\n          (click)="Playback()"\n        ></ion-icon>\n        <ion-icon\n          color="light"\n          name="fastforward"\n          style="font-size:19px;margin-top:11px;margin-right: 17px"\n          (click)="inter(\'fast\')"\n        ></ion-icon>\n      </div>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-item *ngIf="showDropDown">\n    <ion-label>{{ SelectVehicle }}</ion-label>\n    <select-searchable\n      item-content\n      [(ngModel)]="selectedVehicle"\n      [items]="portstemp"\n      itemValueField="Device_Name"\n      itemTextField="Device_Name"\n      [canSearch]="true"\n      (onChange)="onChangedSelect(selectedVehicle)"\n    >\n    </select-searchable>\n  </ion-item>\n  <ion-row>\n    <ion-col width-50 padding-left class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left" />\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">{{ "From Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD/MM/YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeStart"\n          (ionChange)="changeformat(datetimeStart)"\n          style="font-size: 10px;"\n        ></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-50 class="col1">\n      <ion-avatar item-start class="avtar">\n        <img src="assets/imgs/clock.svg" align="left" />\n      </ion-avatar>\n      <ion-label style="margin-top: 1px;">\n        <span style="font-size: 11px">{{ "To Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD/MM/YYYY hh:mm a"\n          pickerFormat="DD/MM/YY hh:mm a"\n          [(ngModel)]="datetimeEnd"\n          (ionChange)="changeformat(datetimeEnd)"\n          style="font-size: 10px;"\n        ></ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col ion-text text-right padding-right>\n      <ion-icon\n        ios="ios-search"\n        md="md-search"\n        style="font-size:30px;"\n        (tap)="btnClicked(datetimeStart, datetimeEnd)"\n      >\n      </ion-icon>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content>\n  <div id="map_canvas">\n    <ion-fab top right>\n      <button ion-fab color="light" mini (click)="onClickMainMenu()">\n        <ion-icon color="gpsc" name="map"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n          S\n        </button>\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n          T\n        </button>\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n          N\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <ion-fab top left>\n      <button ion-fab color="light" mini>\n        <ion-icon color="gpsc" name="arrow-round-forward"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="stoppages()" color="gpsc">\n          P\n        </button>\n        <button ion-fab (click)="onIdle()" color="gpsc">\n          I\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n    <ion-fab\n      style="right: calc(10px + env(safe-area-inset-right)); margin-top: 68%"\n      *ngIf="showZoom"\n    >\n      <button ion-fab mini (click)="zoomin()" color="gpsc">\n        <ion-icon name="add" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab\n      style="right: calc(10px + env(safe-area-inset-right)); margin-top: 80%"\n      *ngIf="showZoom"\n    >\n      <button ion-fab mini (click)="zoomout()" color="gpsc">\n        <ion-icon name="remove" color="black"></ion-icon>\n      </button>\n    </ion-fab>\n  </div>\n</ion-content>\n\n<div *ngIf="showActionSheet" class="divPlan">\n  <!-- <ion-bottom-drawer\n    (click)="setDocHeight()"\n    [(hidden)]="drawerHidden1"\n    [minimumHeight]="minimumHeight"\n    [dockedHeight]="dockedHeight"\n    [bounceThreshold]="bounceThreshold"\n    [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop"\n  > -->\n  <ion-bottom-drawer\n    [(state)]="drawerState"\n    [dockedHeight]="dockedHeight"\n    [shouldBounce]="shouldBounce"\n    [distanceTop]="distanceTop"\n    [transition]="transition"\n    [minimumHeight]="minimumHeight"\n    (click)="setDocHeight()"\n  >\n    <div class="drawer-content">\n      <ion-row style="margin-bottom:-10%;">\n        <ion-col col-12 text-right>\n          <ion-icon style="font-size: 1em; font-weight: bold;" name="close" (click)="closeDocker()"></ion-icon>\n        </ion-col>\n        <ion-col style="text-align:center;">\n          <p\n            style="font-size: 20px;color:black;text-align: center;"\n            *ngIf="!durations"\n          >\n            N/A\n          </p>\n          <p\n            style="font-size: 20px;color:black;text-align: center;"\n            *ngIf="durations"\n          >\n            {{ durations }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row style="margin-bottom: -6%;">\n        <ion-col col-50>\n          <p\n            style="font-size: 13px;color:green;margin-left: 4%;"\n            *ngIf="!arrTime"\n          >\n            <ion-icon\n              name="time"\n              width="55"\n              height="55"\n              style="margin-top: 9%;color: green;"\n            ></ion-icon\n            >&nbsp;&nbsp;N/A\n          </p>\n          <p\n            style="font-size: 13px;color:green;margin-left: 4%;"\n            *ngIf="arrTime"\n          >\n            <ion-icon\n              name="time"\n              width="55"\n              height="55"\n              style="margin-top: 9%;color:green;"\n            ></ion-icon>\n            &nbsp;&nbsp;{{ arrTime }}\n          </p>\n        </ion-col>\n        <ion-col col-50>\n          <p\n            style="font-size: 13px;color:#ac0031;margin-left: 4%;"\n            *ngIf="!depTime"\n          >\n            <ion-icon\n              name="time"\n              width="55"\n              height="55"\n              style="margin-top: 9%;color:#ac0031;"\n            ></ion-icon\n            >&nbsp;&nbsp;N/A\n          </p>\n          <p\n            style="font-size: 13px;margin-left: 4%;color:#ac0031;"\n            *ngIf="depTime"\n          >\n            <ion-icon\n              name="time"\n              width="55"\n              height="55"\n              style="margin-top: 9%;color: #ac0031"\n            ></ion-icon>\n            &nbsp;&nbsp;{{ depTime }}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <p\n          style="font-size: 13px; color:cornflowerblue; margin-left: 4%;"\n          *ngIf="!address"\n        >\n          <ion-icon\n            name="pin"\n            width="55"\n            height="55"\n            style="margin-top: 6%"\n          ></ion-icon>\n          &nbsp;&nbsp;N/A\n        </p>\n        <p\n          style="font-size: 13px; color:cornflowerblue; margin-left: 4%;"\n          *ngIf="address"\n        >\n          <ion-icon\n            name="pin"\n            width="55"\n            height="55"\n            style="margin-top: 9%"\n          ></ion-icon\n          >&nbsp;&nbsp;{{ address }}\n        </p>\n      </ion-row>\n    </div>\n  </ion-bottom-drawer>\n</div>\n<ion-footer class="footSty">\n  <ion-row style="background-color: #dfdfdf; padding: 0px !important;">\n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px; text-align:center;">\n        <ion-icon name="time" style="color:#33cd5f;font-size:15px;"></ion-icon\n        >&nbsp;\n        <span *ngIf="updatetimedate">{{ updatetimedate }}&nbsp;</span>\n        <span *ngIf="!updatetimedate">0:0&nbsp;</span>\n      </p>\n    </ion-col>\n\n    <ion-col width-50 style="padding: 0px">\n      <p style="color:black;font-size:14px;text-align:center;">\n        <ion-icon name="speedometer" style="color:#cd4343"></ion-icon>&nbsp;\n        <span *ngIf="speedMarker"\n          >{{ speedMarker }} {{ "Km/hr" | translate }}</span\n        >\n        <span *ngIf="!speedMarker">0 {{ "Km/hr" | translate }}</span>\n      </p>\n    </ion-col>\n  </ion-row>\n\n  <ion-toolbar>\n    <ion-row no-padding>\n      <ion-col\n        width-50\n        style="text-align: center; border-right: 1px solid white; padding: 0px !important"\n      >\n        <p style="color: white; margin:0px; padding:0px" *ngIf="data2">\n          {{ data2.Distance }} {{ "Kms" | translate }}\n        </p>\n        <p style="color: white; margin:0px; padding:0px" *ngIf="!data2">\n          0 {{ "Kms" | translate }}\n        </p>\n        <p style="color: white; margin:0px; padding:0px">\n          {{ "Total" | translate }} {{ "Distance" | translate }}\n        </p>\n      </ion-col>\n      <ion-col width-50 style="text-align: center; padding: 0px !important">\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="data2">\n          {{ data2.AverageSpeed }} ({{ "Km/hr" | translate }})\n        </p>\n        <p style="color:#ffffff; margin:0px; padding:0px" *ngIf="!data2">\n          0 ({{ "Km/hr" | translate }})\n        </p>\n        <p style="color:#ffffff; margin:0px; padding:0px">\n          {{ "Average Speed" | translate }}\n        </p></ion-col\n      >\n    </ion-row>\n  </ion-toolbar>\n</ion-footer>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/history-device/history-device.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Events"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_5__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"]])
    ], HistoryDevicePage);
    return HistoryDevicePage;
}());

//# sourceMappingURL=history-device.js.map

/***/ })

});
//# sourceMappingURL=32.js.map