webpackJsonp([9],{

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IgnitionReportPageModule", function() { return IgnitionReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ignition_report__ = __webpack_require__(646);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(647);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var IgnitionReportPageModule = /** @class */ (function () {
    function IgnitionReportPageModule() {
    }
    IgnitionReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__ignition_report__["a" /* IgnitionReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ]
        })
    ], IgnitionReportPageModule);
    return IgnitionReportPageModule;
}());

//# sourceMappingURL=ignition-report.module.js.map

/***/ }),

/***/ 646:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IgnitionReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var IgnitionReportPage = /** @class */ (function () {
    function IgnitionReportPage(navCtrl, navParams, apicalligi, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.igiReport = [];
        this.igiReportData = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        console.log("email => " + this.islogin._id);
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
    }
    IgnitionReportPage.prototype.ngOnInit = function () {
        this.getdevices();
    };
    IgnitionReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalligi.startLoading().present();
        this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    IgnitionReportPage.prototype.getIgnitiondevice = function (selectedVehicle) {
        console.log("selectedVehicle=> ", selectedVehicle);
        this.Ignitiondevice_id = selectedVehicle.Device_Name;
    };
    IgnitionReportPage.prototype.getIgnitiondeviceReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicalligi.startLoading().present();
        this.apicalligi.getIgiApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.igiReport = data;
            if (_this.igiReport.length > 0) {
                _this.innerFunc(_this.igiReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: 'Report(s) not found for selected Dates/Vehicle.',
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicalligi.stopLoading();
            console.log(error);
        });
    };
    IgnitionReportPage.prototype.innerFunc = function (igiReport) {
        var outerthis = this;
        outerthis.igiReportData = [];
        outerthis.locationEndAddress = undefined;
        var i = 0, howManyTimes = igiReport.length;
        function f() {
            outerthis.igiReportData.push({
                'vehicleName': outerthis.igiReport[i].vehicleName,
                'switch': outerthis.igiReport[i].switch,
                'timestamp': outerthis.igiReport[i].timestamp,
                'start_location': {
                    'lat': outerthis.igiReport[i].lat,
                    'long': outerthis.igiReport[i].long
                }
            });
            outerthis.start_address(outerthis.igiReport[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    IgnitionReportPage.prototype.start_address = function (item, index) {
        var that = this;
        that.igiReportData[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.igiReportData[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                that.igiReportData[index].StartLocation = str;
            });
        }
    };
    IgnitionReportPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicalligi.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    IgnitionReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-ignition-report',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/ignition-report/ignition-report.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{ "Ignition Report" | translate }}</ion-title>\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;">\n    <ion-label style="margin-top: 15px;">{{\n      "Select Vehicle" | translate\n    }}</ion-label>\n    <select-searchable\n      item-content\n      [(ngModel)]="selectedVehicle"\n      [items]="portstemp"\n      itemValueField="Device_Name"\n      itemTextField="Device_Name"\n      [canSearch]="true"\n      (onChange)="getIgnitiondevice(selectedVehicle)"\n    >\n    </select-searchable>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "From Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD-MM-YYYY hh:mm a"\n          [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{ "To Date" | translate }}</span>\n        <ion-datetime\n          displayFormat="DD-MM-YYYY hh:mm a"\n          pickerFormat="DD-MM-YYYY hh:mm a"\n          [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"\n        >\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon\n          ios="ios-search"\n          md="md-search"\n          style="font-size:2.3em;"\n          (click)="getIgnitiondeviceReport()"\n        >\n        </ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-card *ngFor="let ignitationdata of igiReportData; let i = index">\n      <ion-item style="border-bottom: 2px solid #dedede;">\n        <ion-avatar item-start>\n          <img src="assets/imgs/car_red_icon.png" />\n        </ion-avatar>\n        <ion-label>{{ ignitationdata.vehicleName }}</ion-label>\n        <ion-badge item-end color="gpsc" *ngIf="ignitationdata.switch == \'OFF\'">\n          <ion-icon ios="ios-switch" md="md-switch"></ion-icon>\n        </ion-badge>\n        <ion-badge\n          item-end\n          color="secondary"\n          *ngIf="ignitationdata.switch == \'ON\'"\n        >\n          <ion-icon ios="ios-switch" md="md-switch"></ion-icon>\n        </ion-badge>\n      </ion-item>\n      <ion-card-content>\n        <ion-row style="padding-top:5px;">\n          <ion-col col-1>\n            <ion-icon\n              ios="ios-time"\n              md="md-time"\n              style="font-size:15px;"\n            ></ion-icon>\n          </ion-col>\n          <ion-col\n            col-11\n            style="color:gray;font-size:11px;font-weight: 400;text-align:justify;"\n          >\n            {{ ignitationdata.timestamp | date: "medium" }}</ion-col\n          >\n        </ion-row>\n        <ion-row>\n          <ion-col col-1>\n            <ion-icon\n              ios="ios-pin"\n              md="md-pin"\n              style="color:#e14444;font-size:15px;"\n            ></ion-icon>\n          </ion-col>\n          <ion-col col-11 (onCreate)="start_address(ignitationdata, i)"  style="font-size: 0.8em;">\n            <div class="overme">\n              {{ ignitationdata.StartLocation }}\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/ignition-report/ignition-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], IgnitionReportPage);
    return IgnitionReportPage;
}());

//# sourceMappingURL=ignition-report.js.map

/***/ }),

/***/ 647:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=9.js.map