webpackJsonp([38],{

/***/ 559:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FastagListPageModule", function() { return FastagListPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fastag_list__ = __webpack_require__(629);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FastagListPageModule = /** @class */ (function () {
    function FastagListPageModule() {
    }
    FastagListPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__fastag_list__["a" /* FastagListPage */]),
            ],
        })
    ], FastagListPageModule);
    return FastagListPageModule;
}());

//# sourceMappingURL=fastag-list.module.js.map

/***/ }),

/***/ 629:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FastagListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FastagListPage = /** @class */ (function () {
    function FastagListPage(navCtrl, navParams, apiCall) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.fastagList = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    }
    FastagListPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FastagListPage');
    };
    FastagListPage.prototype.ngOnInit = function () {
        this.getList();
    };
    FastagListPage.prototype.addFastag = function () {
        this.navCtrl.push('FastagPage');
    };
    FastagListPage.prototype.getList = function () {
        var _this = this;
        var url = this.apiCall.mainUrl + 'fastTag/getRequest?id=' + this.islogin.supAdmin + '&role=supAdmin';
        this.apiCall.startLoading().present();
        this.apiCall.getSOSReportAPI(url)
            .subscribe(function (respData) {
            _this.apiCall.stopLoading();
            console.log('respData: ', respData);
            if (respData.length > 0) {
                _this.fastagList = respData;
            }
        }, function (err) {
            _this.apiCall.stopLoading();
        });
    };
    FastagListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fastag-list',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/fastag-list/fastag-list.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Fastag List</ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding>\n  <ion-list>\n    <ion-item *ngFor="let item of fastagList">\n      <ion-avatar item-start>\n        <!-- <img src="assets/imgs/fastag/{{item.vehicle_type}}.png" /> -->\n        <img *ngIf="item.vehicle_type == \'Truck\' || item.vehicle_type == \'Other\'" src="assets/imgs/fastag/Truck.png" />\n        <img *ngIf="item.vehicle_type == \'Bus\'" src="assets/imgs/fastag/bus.png" />\n        <img *ngIf="item.vehicle_type == \'Car\'" src="assets/imgs/fastag/car.png" />\n        <img *ngIf="item.vehicle_type == \'Taxi\'" src="assets/imgs/fastag/cab.png" />\n        <img *ngIf="item.vehicle_type == \'Bike\'" src="assets/imgs/fastag/motorcycle.png" />\n      \n      </ion-avatar>\n      <ion-row>\n        <ion-col col-12>\n          <h3>{{item.vehicle_type}}</h3>\n        </ion-col>\n        <ion-col col-12>\n          <p>\n            <ion-icon name="calendar"></ion-icon>&nbsp;&nbsp;{{item.Date | date:\'medium\'}}\n          </p>\n        </ion-col>\n      </ion-row>\n      <ion-badge item-end>Quantity - {{item.quantity}}</ion-badge>\n    </ion-item>\n  </ion-list>\n\n  <ion-fab right bottom>\n    <button ion-fab color="gpsc" (click)="addFastag()">\n      <ion-icon name="add"></ion-icon>\n    </button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/fastag-list/fastag-list.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], FastagListPage);
    return FastagListPage;
}());

//# sourceMappingURL=fastag-list.js.map

/***/ })

});
//# sourceMappingURL=38.js.map