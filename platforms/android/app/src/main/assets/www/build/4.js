webpackJsonp([4],{

/***/ 597:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripReportPageModule", function() { return TripReportPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__trip_report__ = __webpack_require__(673);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(674);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var TripReportPageModule = /** @class */ (function () {
    function TripReportPageModule() {
    }
    TripReportPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__trip_report__["a" /* TripReportPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], TripReportPageModule);
    return TripReportPageModule;
}());

//# sourceMappingURL=trip-report.module.js.map

/***/ }),

/***/ 673:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TripReportPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TripReportPage = /** @class */ (function () {
    function TripReportPage(navCtrl, navParams, apicalligi, alertCtrl, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalligi = apicalligi;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.portstemp = [];
        this.allData = {};
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    TripReportPage.prototype.ngOnInit = function () {
        if (this.vehicleData == undefined) {
            this.getdevices();
        }
        else {
            this.device_id = this.vehicleData._id;
            this.getTripReport();
        }
    };
    TripReportPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicalligi.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicalligi.startLoading().present();
        this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalligi.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
            _this.isdevice = localStorage.getItem('devices1243');
        }, function (err) {
            _this.apicalligi.stopLoading();
            console.log(err);
        });
    };
    TripReportPage.prototype.getTripdevice = function (item) {
        this.device_id = item._id;
        console.log("device id=> " + this.device_id);
        this.did = item.Device_ID;
        localStorage.setItem('devices_id', item);
        this.isdeviceTripreport = localStorage.getItem('devices_id');
    };
    TripReportPage.prototype.getTripReport = function () {
        var _this = this;
        this.TripReportData = [];
        if (this.datetimeEnd <= this.datetimeStart && this.device_id) {
            var alert_1 = this.alertCtrl.create({
                message: 'To time is always greater than From Time',
                buttons: ['OK']
            });
            alert_1.present();
        }
        else {
            this.apicalligi.startLoading().present();
            this.apicalligi.trip_detailCall(this.islogin._id, new Date(this.datetimeStart).toISOString(), new Date(this.datetimeEnd).toISOString(), this.device_id)
                .subscribe(function (data) {
                _this.apicalligi.stopLoading();
                _this.TripsdataAddress = [];
                if (data.length > 0) {
                    _this.tripFunction(data);
                }
                else {
                    var toast = _this.toastCtrl.create({
                        message: "Report(s) not found for selected dates/Vehicles.",
                        duration: 1500,
                        position: "bottom"
                    });
                    toast.present();
                }
            }, function (err) {
                _this.apicalligi.stopLoading();
                console.log(err);
            });
        }
    };
    TripReportPage.prototype.tripFunction = function (data) {
        var that = this;
        var i = 0, howManyTimes = data.length;
        function f() {
            var deviceId = data[i]._id;
            var distanceBt = data[i].distance / 1000;
            var gmtDateTime = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].start_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Startetime = gmtDateTime.local().format(' h:mm a');
            var Startdate = gmtDate.format('ll');
            var gmtDateTime1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).split('T')[1].split('.')[0], "HH:mm:ss");
            var gmtDate1 = __WEBPACK_IMPORTED_MODULE_3_moment__["utc"](JSON.stringify(data[i].end_time).slice(0, -1).split('T'), "YYYY-MM-DD");
            var Endtime = gmtDateTime1.local().format(' h:mm a');
            var Enddate = gmtDate1.format('ll');
            var startDate = new Date(data[i].start_time).toLocaleString();
            var endDate = new Date(data[i].end_time).toLocaleString();
            var fd = new Date(startDate).getTime();
            var td = new Date(endDate).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs' + ' ' + rminutes + 'mins';
            that.TripReportData.push({
                'Device_Name': data[i].device.Device_Name,
                'Device_ID': data[i].device.Device_ID,
                'Startetime': Startetime,
                'Startdate': Startdate,
                'Endtime': Endtime,
                'Enddate': Enddate,
                'distance': distanceBt,
                '_id': deviceId,
                'start_time': data[i].start_time,
                'end_time': data[i].end_time,
                'duration': Durations,
                'end_location': {
                    'lat': data[i].end_lat,
                    'long': data[i].end_long
                },
                'start_location': {
                    'lat': data[i].start_lat,
                    'long': data[i].start_long
                }
            });
            that.start_address(that.TripReportData[i], i);
            that.end_address(that.TripReportData[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    TripReportPage.prototype.start_address = function (item, index) {
        var that = this;
        that.TripReportData[index].StartLocation = "N/A";
        if (!item.start_location) {
            that.TripReportData[index].StartLocation = "N/A";
        }
        else if (item.start_location) {
            this.geocoderApi.reverseGeocode(Number(item.start_location.lat), Number(item.start_location.long))
                .then(function (res) {
                console.log("test", res);
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.start_location.lat, item.start_location.long);
                that.TripReportData[index].StartLocation = str;
            });
        }
    };
    TripReportPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicalligi.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    TripReportPage.prototype.end_address = function (item, index) {
        var that = this;
        that.TripReportData[index].EndLocation = "N/A";
        if (!item.end_location) {
            that.TripReportData[index].EndLocation = "N/A";
        }
        else if (item.end_location) {
            this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
                that.TripReportData[index].EndLocation = str;
            });
        }
    };
    TripReportPage.prototype.tripReview = function (tripData) {
        this.navCtrl.push('TripReviewPage', {
            params: tripData,
            device_id: this.did
        });
    };
    TripReportPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trip-report',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/trip-report/trip-report.html"*/'<ion-header>\n\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Trip Report\' | translate}}</ion-title>\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;" *ngIf="portstemp.length != 0">\n    <ion-label style="margin-top: 15px;">{{"Select Vehicle" | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getTripdevice(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{"From Date" | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{"To Date" | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;">\n        </ion-datetime>\n      </ion-label>\n    </ion-col>\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;" (click)="getTripReport();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n\n<ion-content [ngClass]="{ \'masters\': portstemp.length != 0,\'masters1\': portstemp.length == 0 }">\n\n  <ion-card *ngFor="let tripdata of TripReportData; let i = index" style="border-radius: 5px;" (click)="tripReview(tripdata)">\n    <ion-item>\n      <ion-label item-start>{{tripdata.Device_Name}}</ion-label>\n      <ion-badge item-end color="gpsc">{{tripdata.distance | number : \'1.0-2\'}}{{"Kms" | translate}}</ion-badge>\n    </ion-item>\n    <ion-card-content>\n      <ion-row>\n        <ion-col col-8 padding-right>\n          <ion-row style="height: 42px; overflow: hidden;text-overflow: ellipsis;">\n            <ion-col col-1>\n              <ion-icon name="pin" color="secondary" style="font-size: 15px;"></ion-icon>\n            </ion-col>\n            <ion-col col-11 (onCreate)="start_address(tripdata, i)"  style="font-size: 0.8em;">\n              <div class="overme">\n                {{ tripdata.StartLocation }}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style="height: 42px; overflow: hidden;text-overflow: ellipsis;">\n            <ion-col col-1>\n              <ion-icon name="pin" color="danger" style="font-size: 15px;"></ion-icon>\n            </ion-col>\n            <ion-col col-11 (onCreate)="end_address(tripdata, i)"  style="font-size: 0.8em;">\n              <div class="overme">\n                {{ tripdata.EndLocation }}\n              </div>\n            </ion-col>\n          </ion-row>\n\n        </ion-col>\n        <ion-col col-4>\n          <ion-thumbnail item-end>\n            <img src="assets/imgs/5Jzwb.png" height="70" width="70">\n          </ion-thumbnail>\n        </ion-col>\n      </ion-row>\n\n      <!-- <ion-row>\n        <ion-col col-8 padding-right>\n          <ion-row>\n              <ion-col style="font-size:1.2em;"><b>{{tripdata.Device_Name}}</b></ion-col>\n          </ion-row>\n          <ion-row style="height: 45px; overflow: hidden;text-overflow: ellipsis;">\n            <ion-col col-2>\n              <ion-icon name="pin" color="secondary" style="font-size: 25px;"></ion-icon>\n            </ion-col>\n            <ion-col col-10>\n              <p>{{tripdata.startAddress}}</p>\n            </ion-col>\n          </ion-row>\n          <ion-row style="height: 45px; overflow: hidden;text-overflow: ellipsis;">\n            <ion-col col-2>\n              <ion-icon name="pin" color="danger" style="font-size: 25px;"></ion-icon>\n            </ion-col>\n            <ion-col col-10>\n              <p>{{tripdata.endAddress}}</p>\n            </ion-col>\n          </ion-row>\n\n        </ion-col>\n        <ion-col col-4>\n          <ion-thumbnail item-end>\n            <img src="assets/imgs/5Jzwb.png">\n          </ion-thumbnail>\n        </ion-col>\n      </ion-row>\n      <ion-row style="padding-top: 10px;">\n        <ion-col col-6 class="divS">\n          <p style="padding: 0px; margin: 0px;" *ngIf="tripdata.distance">\n            <span>DISTNACE </span>\n            <b>{{tripdata.distance | number : \'1.0-2\'}}KM</b>\n          </p>\n          <p style="padding: 0px; margin: 0px;" *ngIf="!tripdata.distance">\n            <span>DISTNACE </span><b>0KM</b></p>\n        </ion-col>\n        <ion-col col-6 class="divS">\n          <p *ngIf="tripdata.duration">\n            <span>DURATION </span>\n            <b>{{tripdata.duration}}</b>\n          </p>\n          <p *ngIf="!tripdata.duration">\n            <span>DURATION </span><b>0hrs 0mins</b></p>\n        </ion-col>\n      </ion-row> -->\n    </ion-card-content>\n    <!-- <ion-row style="background-color:#f2f2f2; padding-top:5px; padding-bottom: 5px" padding-left padding-right>\n      <p style="font-size:0.85em; color:gray; font-weight: bold">{{tripdata.Startdate}}</p>\n    </ion-row> -->\n    <ion-row style="background-color:#f2f2f2; padding-top:5px; padding-bottom: 5px" padding-left padding-right>\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: left;">{{tripdata.Startdate}}</ion-col>\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: right;" *ngIf="tripdata.duration">\n        DURATION&nbsp;{{tripdata.duration}}</ion-col>\n      <ion-col col-6 style=" color:gray; font-weight: bold; text-align: right;" *ngIf="!tripdata.duration">\n        DURATION&nbsp;0hrs 0mins</ion-col>\n    </ion-row>\n  </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/trip-report/trip-report.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], TripReportPage);
    return TripReportPage;
}());

//# sourceMappingURL=trip-report.js.map

/***/ }),

/***/ 674:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=4.js.map