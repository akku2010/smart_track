webpackJsonp([5],{

/***/ 593:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoppagesRepoPageModule", function() { return StoppagesRepoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__ = __webpack_require__(667);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__dummy_directive__ = __webpack_require__(668);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var StoppagesRepoPageModule = /** @class */ (function () {
    function StoppagesRepoPageModule() {
    }
    StoppagesRepoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */],
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__stoppages_repo__["a" /* StoppagesRepoPage */]),
                __WEBPACK_IMPORTED_MODULE_3_ionic_select_searchable__["SelectSearchableModule"],
                __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_5__dummy_directive__["a" /* OnCreate */]
            ],
        })
    ], StoppagesRepoPageModule);
    return StoppagesRepoPageModule;
}());

//# sourceMappingURL=stoppages-repo.module.js.map

/***/ }),

/***/ 667:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoppagesRepoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var StoppagesRepoPage = /** @class */ (function () {
    function StoppagesRepoPage(navCtrl, navParams, apicallStoppages, toastCtrl, geocoderApi) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicallStoppages = apicallStoppages;
        this.toastCtrl = toastCtrl;
        this.geocoderApi = geocoderApi;
        this.portstemp = [];
        this.islogin = JSON.parse(localStorage.getItem('details')) || {};
        this.datetimeStart = __WEBPACK_IMPORTED_MODULE_3_moment__({ hours: 0 }).format();
        this.datetimeEnd = __WEBPACK_IMPORTED_MODULE_3_moment__().format(); //new Date(a).toISOString();
        if (navParams.get('param') != null) {
            this.vehicleData = navParams.get('param');
        }
    }
    StoppagesRepoPage.prototype.ngOnInit = function () {
        if (this.vehicleData == undefined) {
            this.getdevices();
        }
        else {
            this.Ignitiondevice_id = this.vehicleData._id;
            this.getStoppageReport();
        }
    };
    StoppagesRepoPage.prototype.getdevices = function () {
        var _this = this;
        var baseURLp = this.apicallStoppages.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
        if (this.islogin.isSuperAdmin == true) {
            baseURLp += '&supAdmin=' + this.islogin._id;
        }
        else {
            if (this.islogin.isDealer == true) {
                baseURLp += '&dealer=' + this.islogin._id;
            }
        }
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.devices = data;
            _this.portstemp = data.devices;
        }, function (err) {
            _this.apicallStoppages.stopLoading();
            console.log(err);
        });
    };
    StoppagesRepoPage.prototype.getStoppagesdevice = function (data) {
        console.log("selectedVehicle=> ", data);
        this.Ignitiondevice_id = data._id;
    };
    StoppagesRepoPage.prototype.getStoppageReport = function () {
        var _this = this;
        if (this.Ignitiondevice_id == undefined) {
            this.Ignitiondevice_id = "";
        }
        var that = this;
        this.apicallStoppages.startLoading().present();
        this.apicallStoppages.getStoppageApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
            .subscribe(function (data) {
            _this.apicallStoppages.stopLoading();
            _this.stoppagesReport = data;
            _this.Stoppagesdata = [];
            if (_this.stoppagesReport.length > 0) {
                _this.innerFunc(_this.stoppagesReport);
            }
            else {
                var toast = _this.toastCtrl.create({
                    message: "Report(s) not found for selected dates/vehicle.",
                    duration: 1500,
                    position: 'bottom'
                });
                toast.present();
            }
        }, function (error) {
            _this.apicallStoppages.stopLoading();
            console.log(error);
        });
    };
    StoppagesRepoPage.prototype.innerFunc = function (stoppagesReport) {
        var outerthis = this;
        outerthis.locationEndAddress = undefined;
        var i = 0, howManyTimes = stoppagesReport.length;
        function f() {
            var fd = new Date(outerthis.stoppagesReport[i].arrival_time).getTime();
            var td = new Date(outerthis.stoppagesReport[i].departure_time).getTime();
            var time_difference = td - fd;
            var total_min = time_difference / 60000;
            var hours = total_min / 60;
            var rhours = Math.floor(hours);
            var minutes = (hours - rhours) * 60;
            var rminutes = Math.round(minutes);
            var Durations = rhours + 'hrs : ' + rminutes + 'mins';
            outerthis.Stoppagesdata.push({
                'arrival_time': outerthis.stoppagesReport[i].arrival_time,
                'departure_time': outerthis.stoppagesReport[i].departure_time,
                'Durations': Durations,
                'device': outerthis.stoppagesReport[i].device ? outerthis.stoppagesReport[i].device.Device_Name : 'N/A',
                'end_location': {
                    'lat': outerthis.stoppagesReport[i].lat,
                    'long': outerthis.stoppagesReport[i].long
                }
            });
            outerthis.end_address(outerthis.Stoppagesdata[i], i);
            i++;
            if (i < howManyTimes) {
                setTimeout(f, 100);
            }
        }
        f();
    };
    StoppagesRepoPage.prototype.end_address = function (item, index) {
        var that = this;
        that.Stoppagesdata[index].EndLocation = "N/A";
        if (!item.end_location) {
            that.Stoppagesdata[index].EndLocation = "N/A";
        }
        else if (item.end_location) {
            this.geocoderApi.reverseGeocode(Number(item.end_location.lat), Number(item.end_location.long))
                .then(function (res) {
                var str = res.replace(/,\s*$/, ""); //removes last quama in the string using regular expression
                that.saveAddressToServer(str, item.end_location.lat, item.end_location.long);
                that.Stoppagesdata[index].EndLocation = str;
            });
        }
    };
    StoppagesRepoPage.prototype.saveAddressToServer = function (address, lat, lng) {
        var payLoad = {
            "lat": lat,
            "long": lng,
            "address": address
        };
        this.apicallStoppages.saveGoogleAddressAPI(payLoad)
            .subscribe(function (respData) {
            console.log("check if address is stored in db or not? ", respData);
        }, function (err) {
            console.log("getting err while trying to save the address: ", err);
        });
    };
    StoppagesRepoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-stoppages-repo',template:/*ion-inline-start:"/Users/shree/Desktop/white-labels/smart_track/src/pages/stoppages-repo/stoppages-repo.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>{{\'Stoppages Report\' | translate}}</ion-title>\n  </ion-navbar>\n\n  <ion-item style="background-color: #fafafa;" *ngIf="portstemp.length != 0">\n    <ion-label style="margin-top: 15px;">{{\'Select Vehicle\' | translate}}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="getStoppagesdevice(selectedVehicle)">\n    </select-searchable>\n  </ion-item>\n  <ion-row padding-left padding-right style="background-color: #fafafa;">\n    <ion-col width-20>\n      <ion-label>\n        <span style="font-size: 13px">{{\'From Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeStart"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #2ec95c;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20 padding-left>\n      <ion-label>\n        <span style="font-size: 13px">{{\'To Date\' | translate}}</span>\n        <ion-datetime displayFormat="DD-MM-YYYY hh:mm a" pickerFormat="DD/MM/YY hh:mm a" [(ngModel)]="datetimeEnd"\n          style="padding: 7px 2px 0px 0px;font-size:11px;color: #dc0f0f;"></ion-datetime>\n      </ion-label>\n    </ion-col>\n\n    <ion-col width-20>\n      <div style="margin-top: 9px; float: right">\n        <ion-icon ios="ios-search" md="md-search" style="font-size:2.3em;"\n          (click)="getStoppageReport();"></ion-icon>\n      </div>\n    </ion-col>\n  </ion-row>\n</ion-header>\n<ion-content [ngClass]="{ \'masters\': portstemp.length != 0,\'masters1\': portstemp.length == 0 }">\n\n  <ion-card *ngFor="let stopdata of Stoppagesdata;let i = index">\n\n    <ion-item style="border-bottom: 2px solid #dedede;">\n      <ion-avatar item-start>\n        <img src="assets/imgs/car_red_icon.png">\n      </ion-avatar>\n      <ion-label>{{stopdata.device}}</ion-label>\n      <ion-badge item-end color="gpsc">{{stopdata.Durations}}</ion-badge>\n    </ion-item>\n    <ion-card-content>\n      <ion-row style="padding-top:5px">\n        <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;">\n\n          <ion-icon ios="ios-time" md="md-time" style="color:#5edb82; font-size: 15px;"></ion-icon> &nbsp;\n          {{stopdata.arrival_time | date: \'short\'}}\n\n        </ion-col>\n        <ion-col col-6 style="color:gray;font-size:11px;font-weight: 400;">\n\n          <ion-icon ios="ios-time" md="md-time" style="color:#ee7272; font-size: 15px;"></ion-icon>&nbsp;\n          {{stopdata.departure_time | date: \'short\'}}\n\n        </ion-col>\n      </ion-row>\n      <ion-row style="padding-top:5px">\n        <ion-col col-1 style="font-size: 15px;">\n          <ion-icon ios="ios-pin" md="md-pin" style="color:#e14444;"></ion-icon>\n        </ion-col>\n        <ion-col col-11 (onCreate)="end_address(stopdata, i)"  style="font-size: 0.8em;">\n          <div class="overme">\n            {{ stopdata.EndLocation }}\n          </div>\n        </ion-col>\n      </ion-row>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"/Users/shree/Desktop/white-labels/smart_track/src/pages/stoppages-repo/stoppages-repo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ToastController"],
            __WEBPACK_IMPORTED_MODULE_4__providers_geocoder_geocoder__["a" /* GeocoderProvider */]])
    ], StoppagesRepoPage);
    return StoppagesRepoPage;
}());

//# sourceMappingURL=stoppages-repo.js.map

/***/ }),

/***/ 668:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OnCreate; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var OnCreate = /** @class */ (function () {
    function OnCreate() {
        this.onCreate = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    OnCreate.prototype.ngOnInit = function () {
        this.onCreate.emit('dummy');
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
    ], OnCreate.prototype, "onCreate", void 0);
    OnCreate = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
            selector: '[onCreate]'
        }),
        __metadata("design:paramtypes", [])
    ], OnCreate);
    return OnCreate;
}());

//# sourceMappingURL=dummy-directive.js.map

/***/ })

});
//# sourceMappingURL=5.js.map